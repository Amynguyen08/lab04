package linearalgebra;

public class Vector3d{
    private double x;
    private double y;
    private double z;

    public Vector3d(double x, double y, double z){
        this.x = x;
        this.y = y;
        this.z = z;
    }

    public double getX(){
        return this.x;
    }

    public double getY(){
        return this.y;
    }

    public double getZ(){
        return this.z;
    }
    
    public double magnitude(){
        double magnitude = Math.sqrt(x*x + y*y + z*z);
        return magnitude;
    }

    public double dotProduct(Vector3d v){
        double dot = (this.x * v.getX()) + (this.y*v.getY()) + (this.z * v.getZ());  
        return dot;
    }

    public Vector3d add(Vector3d inVector){
        double newX = this.x + inVector.getX();
        double newY = this.y + inVector.getY();
        double newZ = this.z + inVector.getZ(); 
        Vector3d v = new Vector3d(newX, newY, newZ);

        return v;
    }

}