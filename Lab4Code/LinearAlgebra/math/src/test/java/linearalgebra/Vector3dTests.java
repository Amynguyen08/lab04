package linearalgebra;
import static org.junit.Assert.*;
import org.junit.Test;

public class Vector3dTests {
    

    @Test
    public void getVector3dParams(){
        Vector3d v = new Vector3d(2, 5, 4);
        assertEquals(2, v.getX(), 0.0001);
        assertEquals(5, v.getY(), 0.0001);
        assertEquals(4, v.getZ(), 0.00001);
    }

    @Test
    public void checkMagnitudeCorrect(){
        Vector3d v = new Vector3d(2, 5, 4);
        assertEquals(6.70820, v.magnitude(), 0.00001);
    }

    @Test
    public void checkdotProduct_returns46(){
        Vector3d v = new Vector3d(2, 5, 4);
        Vector3d newV = new Vector3d(10, 2, 4);
        assertEquals(46, v.dotProduct(newV), 0.0001);
    }

    @Test
    public void checkAdd(){
        Vector3d v = new Vector3d(2, 5, 4);
        Vector3d newV = new Vector3d(10, 2, 4);
        Vector3d resultV = v.add(newV);
        assertEquals((v.getX() + newV.getX()), resultV.getX(), 0.0001);
        assertEquals((v.getY() + newV.getY()), resultV.getY(), 0.0001);
        assertEquals((v.getZ() + newV.getZ()), resultV.getZ(), 0.0001);
    }

}
